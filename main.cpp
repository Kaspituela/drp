#include <iostream>
#include <fstream>
#include <algorithm>
#include <string.h>
#include <vector>
#include <math.h>
#include <iomanip>
#include <signal.h>

using namespace std;
//Clase para el binario y sus coordenadas.
class OHCA {
  public:
    int x;
    int y;
    int ifAED;
};
//Clase para los AEDs previamente instalados
class AEDPos{
public:
  int* pos;
  int cant;
  int cantO;
};
//Clase para los movimientos de los AEDs
class Movimientos{
public:
  int i;
  int j;
};
//Clase para guardar los mejores valores hasta el momento y el mejor arreglo binario
class Main{
public:
  OHCA* listEventos;
  OHCA* blistEventos;
  int cant;
  int FO;
  vector<int> AEDs;
  int radio;
  Movimientos* movs;
  float budget;
};
//Lee un archivo y crea el arreglo binario para retornarlo
OHCA* iniciarEventos(ifstream* File,int cant){
    string line;
    OHCA* listEventos = new OHCA[cant];
    int i = 0;
    int start,pos;
    while (getline(*File,line)) {
      start = 0;
      pos = line.find(" ");
      listEventos[i].x = stoi(line.substr(start, pos));
      start = ++pos;
      pos = line.find(" ",start);
      listEventos[i].y = stoi(line.substr(start, pos));
      start = ++pos;
      listEventos[i++].ifAED = stoi(line.substr(start, line.size()));
    }
    return listEventos;
}
//Determina si un AED es estático, es decir, no se puede mover
int AEDestatico(AEDPos* AEDExs, int posStart ){
  for (int i = AEDExs->cant; i < AEDExs->cantO; i++) {
    cout << "AEDestatico: " << AEDExs->pos[i] << "\n";
    if (AEDExs->pos[i] == posStart) {
      return 1;
    }
  }
  return 0;
}
//Imprime por consola el arreglo binario
void printList(OHCA* listEventos,int cant){
  for (int i = 0; i < cant; i++) {
    cout << listEventos[i].ifAED << " ";
  }
  cout << "\n";
}
//Retorna 1 si existen cupos suficientes para los AEDs en el arreglo binario , 0 en otro caso
int existeCupo(AEDPos* AEDExs,int cant, int posStart, int numAED){
  int aux = 0;
  for (int i = AEDExs->cant; i < AEDExs->cantO; i++) {
    if (AEDExs->pos[i] > posStart) {
      aux++;
    }
  }
  cout << "cant: " << cant << " posStart: " << posStart << " aux " << aux << "\n";
  if ((cant - posStart - aux) > (AEDExs->cant - numAED)) {
    return 1;
  } else{
    return 0;
  }
}
//Retorna la cantidad de nuevos AEDs para instalar
int cantNewsAED(Movimientos* movs, int cantMovs, float budget){
    float aux = 0.0;
    for (int i = 0; i < cantMovs; i++) {
      if (movs[i].i != movs[i].j) {
        aux = aux + 0.2;
      }
    }
    cout << "aux de NEWSAED: " << aux << "\n";
    int cantAED = int(budget-aux);
    cout << "cantAED: " << cantAED << "\n";
    return cantAED;
}
//Calcula la distancia entre dos puntos (x1,y1) y (x2,y2), retorna 1 si es menor o igual al radio, 0 en otro caso
int distance(int x1, int y1, int x2, int y2, int radio){
  float r = float(sqrt(float(pow(x1-x2,2)) + float(pow(y1-y2,2))));
  if (r <= radio ) {
    return 1;
  }
  return 0;
}
//Calcula la cantidad de eventos OHCA manejados por los AEDs actuales
int calcularFO(Main* main, AEDPos* AEDExs, OHCA* listEventos,Movimientos* movs, int cant){
  int aux = 0;
  vector<int> vecAux;
  for (int i = 0; i < AEDExs->cant; i++) {
    vecAux.push_back(movs[i].j);
  }
  for (int i = AEDExs->cant; i < AEDExs->cantO; i++) {
    vecAux.push_back(movs[i].j);
  }
  for (auto i = main->AEDs.begin(); i != main->AEDs.end(); ++i) {
        vecAux.push_back(*i);
  }
  cout << "Proceso de FO" << "\n";
  for (auto i = vecAux.begin(); i != vecAux.end(); ++i) {
        cout << *i << " ";
  }
  cout << "\n";
  for (int i = 0; i < cant; i++) {
    for (auto j = vecAux.begin(); j != vecAux.end(); j++) {
      if (distance(listEventos[*j].x,listEventos[*j].y,listEventos[i].x,listEventos[i].y,main->radio) == 1) {
        aux++;
        break;
      }
    }
  }
  cout << "Cantidad de OHCA abarcados: " << aux << "\n";
  return aux;
}
//Guarda el arreglo binario y actualiza el valor de eventos cubiertos
void guardar(Main* main, OHCA* listEventos, int cant, int val,AEDPos* AEDExs, Movimientos* movs){
  cout << "Se guarda la lista de evento con val: " << val << "\n";
  for (int i = 0; i < cant; i++) {
    main->blistEventos[i].x = listEventos[i].x;
    main->blistEventos[i].y = listEventos[i].y;
    main->blistEventos[i].ifAED = listEventos[i].ifAED;
  }
  main->FO = val;
  for (int i = 0; i < AEDExs->cant; i++) {
    main->movs[i].i = movs[i].i;
    main->movs[i].j = movs[i].j;
  }
}
//Funcion recursiva para agregar nuevos AEDs
void newsAED(Main* main,OHCA* listEventos,int cant, AEDPos* AEDExs, Movimientos* movs, int posStart, int cantAED){
  int val;
  if (cantAED > 1) {
    while (posStart < (cant-1)) {
      if (listEventos[posStart].ifAED == 0) {
        listEventos[posStart].ifAED = 1;
        main->AEDs.push_back(posStart);
        cout << "push de: " << posStart << "\n";
        cantAED--;
        newsAED(main,listEventos,cant,AEDExs,movs,posStart + 1,cantAED);
        cantAED++;
        main->AEDs.pop_back();
        listEventos[posStart].ifAED = 0;
      }
      posStart++;
    }
  } else {
    while (posStart < cant) {
      if (listEventos[posStart].ifAED == 0) {
        listEventos[posStart].ifAED = 1;
        cout << "push de: " << posStart << "\n";
        main->AEDs.push_back(posStart);
        printList(listEventos,cant);
        val = calcularFO(main,AEDExs,listEventos,movs,cant);
        if (val > main->FO) {
          cout << "val: " << val << " FO: "<< main->FO << "\n";
          guardar(main,listEventos,cant,val,AEDExs,movs);
        }
        main->AEDs.pop_back();
        listEventos[posStart].ifAED = 0;
      }
      posStart++;
    }
  }
}
//Setea el arreglo binario en base a los movimientos guardados
OHCA* setearListEventos(OHCA* listEventos, int cant, AEDPos* AEDExs,Movimientos* movs){
  cout << "Se setea la lista de eventos" << "\n";
  OHCA* newlistEventos = new OHCA[cant];
  for (int i = 0; i < cant; i++) {
    newlistEventos[i].x = listEventos[i].x;
    newlistEventos[i].y = listEventos[i].y;
    newlistEventos[i].ifAED = listEventos[i].ifAED;
  }
  for (int i = 0; i < AEDExs->cant; i++) {
    newlistEventos[movs[i].i].ifAED = 0;
    newlistEventos[movs[i].j].ifAED = 1;
  }
  printList(newlistEventos,cant);
  return newlistEventos;
}
//Funcion recursiva para mover los AEDs previamente instalados
void iniciarMovAed(Main* main,OHCA* listEventos,int cant, AEDPos* AEDExs, Movimientos* movs, int posStart, int numAED, float budget){
  cout << "posStart: "<< posStart << " numAED" << numAED << "\n";
  //Los primeros AED se setean
  if (numAED < (AEDExs->cant - 1)) {
    movs[numAED].i = AEDExs->pos[numAED];
    movs[numAED].j = posStart;
    for (int i = 0; i < AEDExs->cant; i++) {
      cout << "1- " << movs[i].i << " " << movs[i].j << " " << numAED << "\n";
    }
    while(posStart < (cant-1)){
      if (existeCupo(AEDExs,cant,posStart,numAED) == 1) {
        posStart++;
        numAED++;
        iniciarMovAed(main,listEventos,cant,AEDExs,movs,posStart,numAED,budget);
        numAED--;
        if (AEDestatico(AEDExs,posStart) == 0) {
          movs[numAED].j = posStart;
          for (int i = 0; i < AEDExs->cant; i++) {
            cout << "2- " << movs[i].i << " " << movs[i].j << " " << numAED << "\n";
          }
        }
      } else{
        break;
      }
    }
  } else{
    movs[numAED].i = AEDExs->pos[numAED];
    while (posStart < cant) {
      if (AEDestatico(AEDExs,posStart) == 0) {
        movs[numAED].j = posStart;
        for (int i = 0; i < AEDExs->cant; i++) {
          cout << "3- "<<movs[i].i << " " << movs[i].j << " " << numAED << "\n";
        }
        int cantAED = cantNewsAED(movs,AEDExs->cant,budget);
        cout << "cantAED nuevos: " << cantAED << "\n";
        vector<int> AEDs;
        main->AEDs = AEDs;
        OHCA* newlistEventos = setearListEventos(listEventos,cant,AEDExs,movs);
        newsAED(main,newlistEventos,cant,AEDExs,movs,posStart,cantAED);
        delete [] newlistEventos;
      }
      posStart++;
    }
    }
}
//Inicia el proceso de BusquedaCompleta
void BusquedaCompleta(Main* main,OHCA* listEventos,int cant, AEDPos* AEDExs, Movimientos* movs, float budget){
    AEDExs->cantO = AEDExs->cant;
    cout << "AEDcant " << AEDExs->cant << " cant " << cant << "\n";
    if (AEDExs->cant > 0) {
      if (AEDExs->cant * 0.2 > budget) {
        AEDExs->cant = int(budget/0.2);
        cout << "Cant de AED" << AEDExs-> cant;
      }
      iniciarMovAed(main,listEventos,cant,AEDExs,movs,0,0,budget);
    } else {
      newsAED(main,listEventos,cant,AEDExs,movs,0,budget);
    }
}
//Muestra por pantalla el arreglo binario
float printListFinal(OHCA* listEventos, int cant, Movimientos* movs, int cantMovs){
  int cantAED=0;
  int auxCantMovs = 0;
  float aux;
  for (int i = 0; i < cant; i++) {
    if (listEventos[i].ifAED == 1) {
      cantAED ++;
      cout << "AED en la posicion x = " << listEventos[i].x << "; y = " << listEventos[i].y;
      for (int j = 0; j < cantMovs; j++) {
        if (movs[j].j == i) {
          if (movs[j].j == movs[j].i) {
            cout << "; no se ha reposicionado";
          } else{
            auxCantMovs++;
            cout << "; reposicionado de la posicion x = " << listEventos[movs[j].i].x << "; y = " << listEventos[movs[j].i].y;
          }
          break;
        }
      }
      cout <<"\n";
    }
  }
  aux = float(cantAED - cantMovs) + (float(auxCantMovs)*0.2);
  return aux;
}

Main vmain;
AEDPos AEDExs;
clock_t startTime;
//Funcion utilizada por el evento Ctrl+c para imprimir por pantalla los resultados obtenidos hasta el momento.
void printSalida(int numSignal){
  clock_t endTime = clock();
  //print
  cout << "Cantidad de eventos cubiertos: " << vmain.FO << "\n";
  cout << "Porcentaje de eventos cubiertos: " << float(float(vmain.FO)/float(vmain.cant)*100) << "\n";
  double time_taken = double(endTime - startTime) / double(CLOCKS_PER_SEC);
  cout << "Tiempo de ejecucion: " << time_taken << setprecision(5) << "\n";
  cout << "Conjunto de AEDs: " << "\n";
  float currentBudget = printListFinal(vmain.blistEventos,vmain.cant,vmain.movs,AEDExs.cant);
  cout << "Presupuesto sobrante: " << vmain.budget - currentBudget << "\n";
  if (numSignal!=-1) {
    exit(numSignal);
  }
}

int main(int argc, char const *argv[]) {
  signal(SIGINT,printSalida);
  string line;
  ifstream File;
  int cant,radio;
  float budget;
  if (argc != 2) {
    cout << "Se solicita como parametro solamente el nombre del archivo\n";
    exit(1);
  }
  File.open(argv[1]);
  getline(File,line);
  int start = 0;
  int pos = line.find(" ");
  cant = stoi(line.substr(start, pos));
  start = ++pos;
  pos = line.find(" ",start);
  budget = stof(line.substr(start, pos));
  start = ++pos;
  radio = stoi(line.substr(start, line.size()));
  cout << cant << " " << budget << " " << radio << "\n";
  OHCA* listEventos = iniciarEventos(&File,cant);
  //Seteo de la clase vmain
  vmain.blistEventos = new OHCA[cant];
  vmain.listEventos = listEventos;
  vmain.cant = cant;
  vmain.FO = 0;
  vmain.radio = radio;
  vmain.budget = budget;
  //Creacion de las posiciones de los AEDs existentes
  AEDExs.cant = 0;
  for (int i = 0; i < cant; i++) {
    if (listEventos[i].ifAED == 1) {
      AEDExs.cant++;
    }
  }
  AEDExs.pos = new int[AEDExs.cant];
  int aux = 0;
  for (int i = 0; i < cant; i++) {
    if (listEventos[i].ifAED == 1) {
      AEDExs.pos[aux++] = i;
    }
  }
  vmain.movs = new Movimientos[AEDExs.cant];
  //Creacion de un arreglo para guardar los movimientos
  Movimientos* movs = new Movimientos[AEDExs.cant];
  //Inicio del proceso de búsqueda completa
  startTime = clock();
  BusquedaCompleta(&vmain,listEventos,cant,&AEDExs,movs,budget);
  printSalida(-1);
  delete [] vmain.blistEventos;
  delete [] vmain.movs;
  delete [] movs;
  delete [] AEDExs.pos;
  delete [] listEventos;
  File.close();
  return 0;
}
